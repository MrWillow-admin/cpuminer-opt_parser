# README #
This is a HTML page that needs to be fed with the output of your cpuminer-opt in a textarea.
In return it tells you how well each thread performs.

### What is this repository for? ###
To get info on cpuminer-opt's output.

### How do I get set up? ###
Safe the html file under whatever name you prefer, then open it in a webbrowser.

(Instruction are on the page itself too)

Next, copy the output of cpuminer-opt and past that into the textarea.

(Windows users, click on the miner commandprompt, press ctrl-a then ENTER). Past is ctrl-v in the textarea.

Then press the button 'summarize', and you get a table that gives for each thead it average hashes/sec.

Under the table you see the total of hashes/sec (all threads added)

### Screenshot ###

![picture](screenshot.jpg)